# Cautions about this tool

## Guidance from the Agile Manifesto

Value individuals collaborating to continually deliver working software that responds to change.

Value this more than executing processes to follow a plan to deliver the comprehensively documented negotiated contract.

The [Agile Manifesto][] would have us value

> * **Individuals and interactions** over processes and tools

> * **Working software** over comprehensive documentation

> * **Customer collaboration** over contract negotiation

> * **Responding to change** over following a plan

So. 

 * Using this tool and the processes it documents may help, but involved individuals interacting is what delivers progress.
 * Documenting what we think might be the requirements or business needs or status of open loops may help, but working software that meets needs is the very best representation of the needs and their solution status. Quality software with good user experiences is itself documentation of its working reality, this is one sense in which it is "working" and "Done". It should be apparent from inspecting software what problems it solves how much, from an end user perspective, from a developer perspective, and from stakeholder perspectives.
 * Making, capturing, and negotiating commitments may help, but the main thing is collaborate with the stakeholders to continually deliver working software implementing valuable change.
 * Planning and following plans may help, but it may help more to learn from the work and from the emerging reality that to follow the plan that couldn't take that subsequently emerging reality into account.

Consider these [principles][Agile Manifesto Principles]:

> * Business people and developers must work together daily throughout the project.

(This processes documented herein have value; collaboration among motivated individuals to do the right thing is even better.)

> * The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.

Conversations. Have them. A lot. In the context of delivering change during this sprint, and teeing up to deliver change in the next sprint.

[Agile Manifesto]: http://agilemanifesto.org/
[Agile Manifesto Principles]: http://agilemanifesto.org/principles.html
