# Sprint Lifecycle Process

## Preparing to plan a Sprint

 * Prioritize the backlog, informed by story point estimates
 * Consider the current quarter roadmap
 * Consider what stories block other downstream stories
 * Consider what commitments of what timelines have been made or implied re stakeholder requests, known bugs

## The Sprint Planning Meeting

Only the Scrum team can commit to a sprint scope. This is a ScrumMaster-facilitated meeting.

Articulate priorities, opportunities for the product to deliver value, to inform the team of the likely consequences (positive and otherwise) of what is and is not committed to in a sprint.

 * Help the team consider opportunities for synergy. It might be worth pulling a story about a same component into the sprint to get more progress into fewer releases. Alternatively, it might be worth deferring a story that would be better informed with the work of a story that is in this sprint.
 * Negotiate scope. There may be opportunities to do less sooner and thereby begin delivering a return on the investment of effort and learn from the work in pursuing more scope.

##  Publishing a sprint plan

JIRA can report what stories are in a sprint and what is and is not done.

The [sprint plan wiki](https://wiki.doit.wisc.edu/confluence/display/MUM/Scrum+Sprints) page puts this in context of *why* this sprint scope is valuable. Relate the sprint scope to themes and to the roadmap. Nonetheless, be brief. Use JIRA issue macros effectively so that the page surfaces both the context and the status of the stories.

Email a link to the sprint plan page to the MyUW Service Team at `mumteam@`. Highlight in the email what this means for MyUW as a service.

Email a link to the sprint plan page to the MyUW developers at `my-dev@`. Highlight in the email what this means for MyUW as a development and delivery platform.

## Invite stakeholders to the sprint review

Navigate the tradeoffs of good uses of peoples' time, options to communicate in other ways, etc., in attendance of the sprint review meeting.

This is challenging in that often a stakeholder will have an item that could be reviewed in two minutes. Attending a whole meeting to review something for two minutes is poor - consider email and other ways of communicating. On the other hand, more voices and perspectives makes for a more informative sprint review.

## Prepare the sprint review

See also Sprint Review in the Scrum guide.

This meeting works better with an agenda. Modeling that as a wiki page child of the sprint wiki page works well.

While items to demo and discuss can emerge at the meeting, it is good to have items prepared in case this does not emerge. You can at least in part predict what is most valuable to review live.

## Transforming a sprint plan to a sprint result

As the sprint draws to a close, update the sprint plan wiki page to instead articulate the sprint result. Reconcile against the JIRA sprint report, noting asterisk-marked stories that were added and stories that were removed or not done.

## The Sprint Review

**The goal of the Sprint Review is to inform the product backlog by learning about the product from the building of the product.** 

Improving stakeholder understanding of progress and informing release engineering, go/no go on promoting change, is also valuable.

 * *Quickly* summarize what was done and not done
 * Give the Scrum team a chance to reflect on what went well and what did not *from a product perspective*
 * Demo and review in depth the most important items. Scrum team members should do this demo. The most important items are controversial or offer lessons or benefit from stakeholder understanding or benefit most from feedback or so.
 * Check in on the Product Backlog.
 * *Briefly* summarize changes external to the sprint that have informed updates to the Product Backlog.
 * Learn from this sprint progress in informing the product backlog. How ought the backlog change based on what is done and not done and what was learned from this work?
 * Check in on the roadmap, on how this sprint relates to current quarter roadmap initiatives.


## Post-processing the sprint review

Follow up on the sprint review to update the product backlog. If there is a wiki page for the sprint review meeting, leave it in a clean state.

Considering the sprint results, consider and execute appropriate communication. Who needs to know about this progress? Who can provide feedback or inform follow-up work?

## Post-processing the sprint result

For the completed stories, how many story points were achieved, of what nature? Summarize this in the sprint wiki page and update [the google spreadsheet][Google spreadsheet for story points per sprint] that drives the graphs.

[Google spreadsheet for story points per sprint]: https://docs.google.com/spreadsheets/d/1uv9mDNmKGBGFOZDOl4UN096RmdaGpL5qvhyoaV_tpyU/edit#gid=0
